var Comm = {
  output: function(size) {
    (this.buffer = new Uint8Array(size)),
      (this.idx = 0),
      (this.packInt8 = function(val) {
        (this.buffer[this.idx] = 255 & val), this.idx++;
      }),
      (this.packInt16 = function(val) {
        (this.buffer[this.idx] = 255 & val),
          (this.buffer[this.idx + 1] = (val >> 8) & 255),
          (this.idx += 2);
      }),
      (this.packInt32 = function(val) {
        (this.buffer[this.idx] = 255 & val),
          (this.buffer[this.idx + 1] = (val >> 8) & 255),
          (this.buffer[this.idx + 2] = (val >> 16) & 255),
          (this.buffer[this.idx + 3] = (val >> 24) & 255),
          (this.idx += 4);
      }),
      (this.packRadU = function(val) {
        this.packInt16(1e4 * val);
      }),
      (this.packRad = function(val) {
        this.packInt16(1e4 * (val + Math.PI));
      }),
      (this.packFloat = function(val) {
        this.packInt16(300 * val);
      }),
      (this.packDouble = function(val) {
        this.packInt32(1e6 * val);
      }),
      (this.packString = function(str) {
        this.packInt8(str.length);
        for (var i = 0; i < str.length; i++) this.packInt16(str.charCodeAt(i));
      });
  },
  input: function(buf) {
    (this.buffer = new Uint8Array(buf)),
      (this.idx = 0),
      (this.isMoreDataAvailable = function() {
        return Math.max(0, this.buffer.length - this.idx);
      }),
      (this.unPackInt8U = function() {
        var i = this.idx;
        return this.idx++, this.buffer[i];
      }),
      (this.unPackInt8 = function() {
        return ((this.unPackInt8U() + 128) % 256) - 128;
      }),
      (this.unPackInt16U = function() {
        var i = this.idx;
        return (this.idx += 2), this.buffer[i] + (this.buffer[i + 1] << 8);
      }),
      (this.unPackInt32U = function() {
        var i = this.idx;
        return (
          (this.idx += 4),
          this.buffer[i] +
            256 * this.buffer[i + 1] +
            65536 * this.buffer[i + 2] +
            16777216 * this.buffer[i + 3]
        );
      }),
      (this.unPackInt16 = function() {
        return ((this.unPackInt16U() + 32768) % 65536) - 32768;
      }),
      (this.unPackInt32 = function() {
        return ((this.unPackInt32U() + 2147483648) % 4294967296) - 2147483648;
      }),
      (this.unPackRadU = function() {
        return this.unPackInt16U() / 1e4;
      }),
      (this.unPackRad = function() {
        return this.unPackRadU() - Math.PI;
      }),
      (this.unPackFloat = function() {
        return this.unPackInt16() / 300;
      }),
      (this.unPackDouble = function() {
        return this.unPackInt32() / 1e6;
      }),
      (this.unPackString = function(maxLen) {
        maxLen = maxLen || 1e3;
        for (
          var len = Math.min(this.unPackInt8U(), maxLen),
            str = new String(),
            i = 0;
          i < len;
          i++
        ) {
          var c = this.unPackInt16U();
          c > 0 && (str += String.fromCharCode(c));
        }
        return str;
      });
  }
};

CommCode = {
  gameJoined: 0,
  addPlayer: 1,
  removePlayer: 2,
  chat: 3,
  controlKeys: 4,
  keyUp: 5,
  sync: 6,
  fire: 7,
  jump: 8,
  die: 9,
  hitThem: 10,
  hitMe: 11,
  collectItem: 12,
  spawnItem: 13,
  respawn: 14,
  startReload: 15,
  reload: 16,
  swapWeapon: 17,
  fireBullet: 18,
  fireShot: 19,
  joinGame: 20,
  ping: 21,
  pong: 22,
  clientReady: 23,
  requestRespawn: 24,
  throwGrenade: 25,
  joinPublicGame: 26,
  joinPrivateGame: 27,
  createPrivateGame: 28,
  gameOver: 29,
  switchTeam: 30,
  firePrecise: 31,
  notification: 32,
  changeCharacter: 33,
  playerCount: 34,
  masterIP: 255
};

var players = {};
var me = {};
var meId, myTeam, gameType, uniqueId, uniqueKey, mapIdx;

var canvas = document.getElementById("canvas");

var windowWidth = canvas.width,
  windowHeight = canvas.height;

var overlayCanvas = document.createElement("canvas");
document.body.appendChild(overlayCanvas);
overlayCanvas.id = "overlayCanvas";
overlayCanvas.style.position = "absolute";
overlayCanvas.style.left = "0px";
overlayCanvas.style.top = "0px";
overlayCanvas.style.width = windowWidth + "px";
overlayCanvas.style.height = windowHeight + "px";
overlayCanvas.style.backgroundColor = "transparent";
overlayCanvas.style.opacity = "1";
overlayCanvas.style.pointerEvents = "none";
overlayCanvas.classList.add("unselectable");

var overlayContext = overlayCanvas.getContext("2d");

canvas.addEventListener(
  "mousemove",
  function(event) {
    console.log(event);
  },
  !1
);

function init() {
  WebSocket.prototype._send = WebSocket.prototype.send;
  WebSocket.prototype.send = function(data) {
    this._send(data);
    this.addEventListener(
      "message",
      function(msg) {
        for (
          var input = new Comm.input(msg.data);
          input.isMoreDataAvailable();

        ) {
          var commCode = input.unPackInt8U();
          switch (commCode) {
            case CommCode.gameJoined: //Just joined
              gameJoined(input);
              break;
            case CommCode.addPlayer: //Add player
              addPlayer(input);
              break;
            case CommCode.removePlayer:
              var playerId = input.unPackInt8U();
              delete players[playerId];
              break;
            case CommCode.sync: //Sync
              syncGame(input);
              break;
          }
        }
      },
      false
    );
    this.send = function(data) {
      this._send(data);
      //console.log("<< " + data);
    };
  };
}

function gameJoined(input) {
  this.meId = input.unPackInt8U();
  this.myTeam = input.unPackInt8U();
  this.gameType = input.unPackInt8U();
  this.uniqueId = input.unPackInt16U();
  this.uniqueKey = input.unPackInt16U();
  this.mapIdx = input.unPackInt8U();
  //console.log("JOINED GAME");
}

function addPlayer(input) {
  //NOt work

  var playerData = {
    id: input.unPackInt8U(),
    name: input.unPackString(),
    charClass: input.unPackInt8U(),
    team: input.unPackInt8U(),
    shellColor: input.unPackInt8U(),
    hatIdx: input.unPackInt8U(),
    stampIdx: input.unPackInt8U(),
    x: input.unPackFloat(),
    y: input.unPackFloat(),
    z: input.unPackFloat(),
    dx: input.unPackFloat(),
    dy: input.unPackFloat(),
    dz: input.unPackFloat(),
    viewYaw: input.unPackRadU(),
    moveYaw: input.unPackRadU(),
    pitch: input.unPackRad(),
    totalKills: input.unPackInt32U(),
    totalDeaths: input.unPackInt32U(),
    killStreak: input.unPackInt16U(),
    bestKillStreak: input.unPackInt16U(),
    shield: input.unPackInt8U(),
    hp: input.unPackInt8U(),
    weaponIdx: input.unPackInt8U(),
    controlKeys: input.unPackInt8U()
  };

  //console.log("adding player " + playerData.id);

  playerData.id == this.meId
    ? (this.me = {
        x: playerData.x,
        y: playerData.y,
        z: playerData.z,
        id: playerData.id,
        pitch: playerData.pitch,
        yaw: playerData.yaw
      })
    : (this.players[playerData.id] = {
        x: playerData.x,
        y: playerData.y,
        z: playerData.z,
        id: playerData.id,
        pitch: playerData.pitch,
        yaw: playerData.yaw
      });
}

function syncGame(input) {
  var id = input.unPackInt8U(),
    stateIdx = input.unPackInt8U(),
    x = input.unPackFloat(),
    y = input.unPackFloat(),
    z = input.unPackFloat(),
    yaw = input.unPackRadU(),
    pitch = input.unPackRad(),
    climbing = input.unPackInt8U();

  if (id != meId) {
    if (!players[id]) {
      players[id] = {};
    }
    players[id].x = x;
    players[id].y = y;
    players[id].z = z;
    players[id].id = id;
    players[id].pitch = pitch;
    players[id].yaw = yaw;
    var distance = calculateDistance(me, players[id]);
    var angles = calculateAngles(me, players[id], distance);

    if (angles && !isNaN(angles.pitch) && !isNaN(angles.yaw)) {
      var distanceScaler = 25 - distance;

      var yawScaled = windowWidth / 2 + angles.yaw * distanceScaler;
      var pitchScaled = windowWidth / 2 + angles.pitch * distanceScaler;

      console.log("Yaw : " + yawScaled + " Pitch: " + pitchScaled);

      dispatchEvent(
        canvas,
        mouseEvent("mousemove", yawScaled, pitchScaled, yawScaled, pitchScaled)
      );
    }
  } else {
    me.x = x;
    me.y = y;
    me.z = z;
    me.id = id;
    me.pitch = pitch;
    me.yaw = yaw;
  }
  //console.log("ID: " + id + " X: " + x + " Y: " + x + " Z: " + z);
}

function calculateDistance(me, them) {
  return Math.sqrt(
    Math.pow(me.x - them.x, 2) +
      Math.pow(me.y - them.y, 2) +
      Math.pow(me.z - them.z, 2)
  );
}

function calculateAngles(me, them, distance) {
  var xInRelation = them.x - me.x;
  var yInRelation = them.y - me.y;
  var zInRelation = them.z - me.z;

  var yaw = (Math.atan2(yInRelation, xInRelation) * 180) / 3.14;
  var pitch = (Math.atan2(zInRelation, distance) * 180) / 3.14;

  return { pitch: pitch, yaw: yaw };
}

function mouseEvent(type, sx, sy, cx, cy) {
  var evt;
  var e = {
    bubbles: true,
    cancelable: false,
    view: window,
    detail: 0,
    screenX: sx,
    screenY: sy,
    clientX: cx,
    clientY: cy,
    ctrlKey: false,
    altKey: false,
    shiftKey: false,
    metaKey: false,
    button: 0,
    relatedTarget: null
  };
  if (typeof document.createEvent == "function") {
    evt = document.createEvent("MouseEvents");
    evt.initMouseEvent(
      type,
      e.bubbles,
      e.cancelable,
      e.view,
      e.detail,
      e.screenX,
      e.screenY,
      e.clientX,
      e.clientY,
      e.ctrlKey,
      e.altKey,
      e.shiftKey,
      e.metaKey,
      e.button,
      document.body.parentNode
    );
  } else if (document.createEventObject) {
    evt = document.createEventObject();
    for (prop in e) {
      evt[prop] = e[prop];
    }
    evt.button = { 0: 1, 1: 4, 2: 2 }[evt.button] || evt.button;
  }
  return evt;
}

function dispatchEvent(el, evt) {
  if (el.dispatchEvent) {
    el.dispatchEvent(evt);
  } else if (el.fireEvent) {
    el.fireEvent("on" + type, evt);
  }
  return evt;
}

init();
